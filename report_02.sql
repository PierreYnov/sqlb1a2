SQL> SELECT c.customer_id, r.buyer_id, c.last_name, c.first_name
  2  FROM t_reservation r
  3  NATURAL JOIN t_customer c
  4  WHERE r.buyer_id != c.customer_id
  5  ORDER BY last_name, first_name;

