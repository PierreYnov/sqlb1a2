SELECT c.last_name , c.first_name, 
    CASE 
        WHEN MONTHS_BETWEEN (SYSDATE, pass_date) <= 12 THEN 'Valide'
        WHEN MONTHS_BETWEEN (SYSDATE, pass_date) > 12 THEN 'Périmé!'
        WHEN pass_id IS NULL THEN 'Aucun'
    END "Titre d'abonnement"
FROM t_customer c;
