SELECT train_id "Numéro du train", depart.city || '-' || arrive.city "Nom du train", p.pass_name, t.price * (1 - p.discount_pct/100) "Tarif semaine en euros", t.price * (1 - p.discount_we_pct/100) "tarif Week-end en euros"
FROM t_train t
    JOIN t_station depart
        ON t.departure_station_id = depart.station_id
    JOIN t_station arrive
        ON t.arrival_station_id = arrive.station_id
    CROSS JOIN t_pass p
WHERE depart.city LIKE 'Paris'
ORDER BY train_id;
