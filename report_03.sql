
SQL> SELECT t.creation_date, t.reservation_id, e.first_name, e.last_name, c.first_name, c.last_name 

  2  FROM t_reservation t
  3  JOIN t_employee e 
  4  ON t.employee_id = e.employee_id
  5  JOIN t_customer c 
  6  ON c.customer_id = t.buyer_id
  7  WHERE t.creation_date = ( 
  8     SELECT min(creation_date) 
  9     FROM t_reservation
 10     )
 11  ;

