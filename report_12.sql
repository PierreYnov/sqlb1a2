SELECT p.pass_name
FROM t_customer c
JOIN t_pass p 
ON c.pass_id = p.pass_id
ORDER BY p.pass_name DESC;