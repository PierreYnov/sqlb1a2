select *
from ( select count(tic.ticket_id) as "Nb de tickets", tr.train_id as "Train", st_d.city || ' - ' || st_a.city as "Depart - Arrive"
	from t_ticket tic
	join t_wagon_train w
	on w.wag_tr_id = tic.wag_tr_id
	join t_train tr
	on w.train_id = tr.train_id 
	join t_station st_a
	on tr.arrival_station_id = st_a.station_id
	join t_station st_d
	on tr.departure_station_id = st_d.station_id
	group by tr.train_id, st_d.city, st_a.city
	order by "Nb de tickets" desc )
where rownum <= 5;
