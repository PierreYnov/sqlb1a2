SELECT tr.departure_time, tr.train_id, tic.ticket_id, cus.last_name as Nom, cus.first_name as Prénom, cus.birth_date as age
FROM T_TICKET tic
JOIN T_CUSTOMER cus ON cus.customer_id = tic.customer_id
JOIN T_WAGON_TRAIN wag ON wag.wag_tr_id = tic.wag_tr_id
JOIN T_TRAIN tr ON tr.train_id = wag.train_id
WHERE trunc(months_between( sysdate, cus.birth_date) /12 ) < trunc(months_between( sysdate,date '1994-01-01') /12 )
ORDER BY cus.pass_date ASC;

select trunc(months_between( sysdate,date '1994-01-01') /12 ) FROM dual;