SELECT count(c.last_name) as "Nombre de séniors"
FROM t_customer c 
    JOIN t_pass p
        ON c.pass_id = p.pass_id
        AND p.pass_name = 'Senior'
    JOIN t_reservation r
        ON r.buyer_id = c.customer_id
            AND r.buy_method IS NOT NULL
    JOIN t_ticket t
        ON t.reservation_id = r.reservation_id
    JOIN t_wagon_train w
        ON w.wag_tr_id = t.wag_tr_id
    JOIN t_train tr
        ON tr.train_id = w.train_id
        AND tr.departure_time LIKE '%/01/19'; /* il n'y a aucun départ en mars dans la table */
