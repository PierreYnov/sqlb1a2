select e.last_name, count(r.reservation_id)
from t_reservation r 
join t_employee e
ON e.employee_id = r.employee_id
group by e.last_name
having count(r.reservation_id) = (select max(res)
								from (select count(reservation_id) as res, employee_id
									 from t_reservation 
									 group by employee_id));