SELECT t.train_id, depart.city || '-' || arrive.city "Nom du train", w.nb_seat
FROM t_train t
    JOIN t_station depart
        ON t.departure_station_id = depart.station_id
    JOIN t_station arrive
        ON t.arrival_station_id = arrive.station_id
    JOIN t_wagon_train wt
        ON wt.train_id = t.train_id
    JOIN t_wagon w
        ON w.wagon_id  = wt.wagon_id
WHERE t.distance > 300
    AND t.departure_time LIKE '22/05/19';